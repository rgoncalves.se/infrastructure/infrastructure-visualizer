#!/usr/bin/env python3
"""
    _       ____                      _                  ___
   (_)___  / __/________ _     _   __(_)______  ______ _/ (_)___  ___  _____
  / / __ \/ /_/ ___/ __ `/    | | / / / ___/ / / / __ `/ / /_  / / _ \/ ___/
 / / / / / __/ /  / /_/ /     | |/ / (__  ) /_/ / /_/ / / / / /_/  __/ /
/_/_/ /_/_/ /_/   \__,_(_)    |___/_/____/\__,_/\__,_/_/_/ /___/\___/_/

 ~~~<rgoncalves.se>
"""


import logging
import argparse
import parser

from dash.dependencies import Input, Output

import dash
import dash_core_components as dcc
import dash_html_components as html
import dash_bootstrap_components as dbc
import networkx as nx

import util


logging.basicConfig(level = logging.DEBUG)


""" ===========================================================================
    FUNCTIONS
    FUNCTIONS
    FUNCTIONS
=========================================================================== """


def show_diagram(figure):
	"""Render the network diagram in a web page."""
	app.title = "network viewer"
	app.layout = dbc.Container(
		className = "p-0",
		children = [
			dbc.NavbarSimple(
				brand = "network visualizer",
				brand_href = "#",
				color = "primary",
				dark = True,
				className = "shadow"
			),
			html.Br(),
			dbc.Container([
				dbc.CardDeck([
					dbc.Card(
						[
							dbc.CardHeader("Network diagram"),
							dbc.CardBody([
								dcc.Graph(id = "network-diagram", figure = figure)
							])
						], className = "shadow-sm"
					),
					dbc.Card(
						[
							dbc.CardHeader("Network informations"),
							dbc.CardBody([
								html.Pre(id = "network-informations")
							])
						], className = "shadow-sm"
					)
				])
			], fluid = True),
			html.Br(),
			dbc.Container([
				dbc.CardDeck([
					dbc.Card(
						[
							dbc.CardHeader("Controls"),
							dbc.CardBody(":/")
						], className = "shadow-sm"
					)
				])
			], fluid = True)
		],
		fluid = True
	)
	app.run_server(debug = True)



def get_args():
	"""Get arguments from user input."""

	description = "Infrastructure and network diagram visualizer."""
	arg = argparse.ArgumentParser(description = description)

	arg.add_argument("--src", const = "src_file", nargs = "?")
	arg.add_argument("--a", action = "store_true")

	return arg


""" ===========================================================================
    SPECIAL VARIABLES
    SPECIAL VARIABLES
    SPECIAL VARIABLES
=========================================================================== """


args = get_args().parse_args()
infrastructure_data = util.get_data(args.src)


# Empty object, allowing generation through function from above
app = dash.Dash(
	__name__,
	external_stylesheets = [
		dbc.themes.BOOTSTRAP
	]
)


""" ===========================================================================
    WEBAPP CALLBACK
    WEBAPP CALLBACK
    WEBAPP CALLBACK
=========================================================================== """


# Diagram hover event
@app.callback(
	Output("network-informations", "children"),
	[
		Input("network-diagram", "clickData")
	]
)
def disp_hover_data(hover_data):
	"""Display data on hover"""

	name = util.get_hovered_hostname(hover_data)
	text = util.get_host_data(name,infrastructure_data)
	table = util.get_information_table(text)
	return table


""" ===========================================================================
    MAIN ENTRY
    MAIN ENTRY
    MAIN ENTRY
=========================================================================== """


if __name__ == "__main__" :
	"""Serves the web application."""

	diagram = nx.OrderedDiGraph()
	util.create_nodes(diagram, infrastructure_data)
	util.create_edges(diagram)

	coords = util.create_coordinates(diagram)
	fig = util.convert_diagram_to_go(diagram, coords)
	show_diagram(fig)



