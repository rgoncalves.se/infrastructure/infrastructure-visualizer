## usage

``` bash
python3 main.py --src infrastructure.yml
```

``` bash
./infrastructure-visualizer/main.py --src infrastructure/infrasctructure.yml
```

## graphical elements

![](example.jpg)

- diagram visualizer with nodes and paths
- table containing sub-keys and values
- controller box with input values
